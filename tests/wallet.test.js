const LotteryService = require("../js/service/lottery-service");
const LotteryState = require("../js/model/lottery-state");
const Token = require("../js/model/token");
const TokenBalance = require("../js/model/token-balance");
const ParticipationRule = require("../js/model/participation-rule");
const TicketRule = require("../js/model/ticket-rule");
const { PlotRange, PlotList } = require("../js/model/reward-spec");
const { getBlockchainBrid } = require("../js/utils");
const { createTestLottery, randomAddress } = require("./utils");

require("dotenv").config();

const url = "http://localhost:7740";

let lotteryService = null;
let brid = null;

describe("Wallet", () => {
  beforeAll(async () => {
    brid = await getBlockchainBrid(url, 0);
    lotteryService = await LotteryService.initialize(url, brid, process.env.ADMIN_PRIV_KEY);
  })

  it("should be able to have secondary wallet", async () => {
    const masterAddress = randomAddress();
    const secondaryAddress = randomAddress();

    await lotteryService.registerMasterWallet(masterAddress);
    await lotteryService.linkWallet(masterAddress, secondaryAddress);

    const secondaryWallets = await lotteryService.getLinkedWallets(masterAddress);

    expect(secondaryWallets).toEqual([secondaryAddress]);
  })

  it("should be able to find its master wallet", async () => {
    const address = randomAddress();
    const secondaryAddress = randomAddress();

    await lotteryService.registerMasterWallet(address);
    await lotteryService.linkWallet(address, secondaryAddress);

    const masterAddress = await lotteryService.getMasterWallet(secondaryAddress);

    expect(address).toEqual(masterAddress);
  })

  it("should be able to check if it is master wallet", async () => {
    const address = randomAddress();
    const secondaryAddress = randomAddress();

    await lotteryService.registerMasterWallet(address);
    await lotteryService.linkWallet(address, secondaryAddress);

    await expect(lotteryService.isMasterWallet(address)).resolves.toBeTruthy();
    await expect(lotteryService.isMasterWallet(secondaryAddress)).resolves.toBeFalsy();
  })

  it("should return linked wallets in wallet status", async () => {
    await createTestLottery(lotteryService);

    const address = randomAddress();
    const secondaryAddress = randomAddress();

    await lotteryService.registerMasterWallet(address);
    await lotteryService.linkWallet(address, secondaryAddress);

    await lotteryService.createNewSnapshottingRound(1621008000000);
    const status = await lotteryService.getActiveLotteryWalletStatus(address);

    expect(status.master_address).toEqual(address);
    expect(status.wallets).toEqual([secondaryAddress]);

    await lotteryService.setLotteryState(LotteryState.Canceled);
  })

  it("should return master wallet when querying secondary wallet", async () => {
    await createTestLottery(lotteryService);

    const address = randomAddress();
    const secondaryAddress = randomAddress();

    await lotteryService.registerMasterWallet(address);
    await lotteryService.linkWallet(address, secondaryAddress);

    await lotteryService.createNewSnapshottingRound(1621008000000);
    const status = await lotteryService.getActiveLotteryWalletStatus(secondaryAddress);

    expect(status.master_address).toEqual(address);
    expect(status.wallets).toEqual([secondaryAddress]);

    await lotteryService.setLotteryState(LotteryState.Canceled);
  })

  it("should be able to get rewards", async () => {
    const address = randomAddress();
    await lotteryService.registerMasterWallet(address);

    await lotteryService.createLottery(
      [new ParticipationRule("ALICE", "BSC", 20)],
      [new TicketRule("ALICE", "BSC", 1, 1)],
      [new PlotList(["ABCDEF"])],
      "This is plot lottery",
      1621008000000
    );
    const lottery1Id = await lotteryService.getActiveLotteryId();
    await lotteryService.createNewSnapshottingRound(1621008000000);
    await lotteryService.setLotteryState(LotteryState.Snapshotting);
    await lotteryService.saveWalletSnapshot(address, [
      new TokenBalance("ALICE", "BSC", 1000)
    ]);
    await lotteryService.setLotteryState(LotteryState.SnapshottingFinished);
    await lotteryService.issueTickets();
    await lotteryService.setLotteryState(LotteryState.WaitingDraw);
    await lotteryService.draw();
    await lotteryService.setLotteryState(LotteryState.DrawFinished);
    await lotteryService.setLotteryState(LotteryState.Finished);

    await lotteryService.createLottery(
      [new ParticipationRule("ALICE", "BSC", 20)],
      [new TicketRule("ALICE", "BSC", 1, 1)],
      [new PlotRange(100, 200)],
      "This is plot lottery",
      1621008000000
    );
    const lottery2Id = await lotteryService.getActiveLotteryId();
    await lotteryService.createNewSnapshottingRound(1621008000000);
    await lotteryService.setLotteryState(LotteryState.Snapshotting);
    await lotteryService.saveWalletSnapshot(address, [
      new TokenBalance("ALICE", "BSC", 100)
    ]);
    await lotteryService.setLotteryState(LotteryState.SnapshottingFinished);
    await lotteryService.issueTickets();
    await lotteryService.setLotteryState(LotteryState.WaitingDraw);
    await lotteryService.draw();
    await lotteryService.setLotteryState(LotteryState.DrawFinished);
    await lotteryService.setLotteryState(LotteryState.Finished);

    await createTestLottery(lotteryService);
    await lotteryService.createNewSnapshottingRound(1621008000000);
    await lotteryService.setLotteryState(LotteryState.Snapshotting);
    await lotteryService.setLotteryState(LotteryState.SnapshottingFinished);
    await lotteryService.issueTickets();
    await lotteryService.setLotteryState(LotteryState.WaitingDraw);
    await lotteryService.draw();
    await lotteryService.setLotteryState(LotteryState.DrawFinished);
    await lotteryService.setLotteryState(LotteryState.Finished);

    const rewards = await lotteryService.getWalletRewards(address);
    expect(rewards).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          lottery_id: lottery1Id,
          reward: {
            type: "plot",
            details: {
              type: "id",
              value: "ABCDEF"
            }
          }
        }),
        expect.objectContaining({
          lottery_id: lottery2Id,
          reward: {
            type: "plot",
            details: {
              type: "range_id",
              value: 100
            }
          }
        })
      ])
    )
  }, 20000)

  it("should be able to get rewards grouped by lottery", async () => {
    const address = randomAddress();
    await lotteryService.registerMasterWallet(address);

    await lotteryService.createLottery(
      [new ParticipationRule("ALICE", "BSC", 20)],
      [new TicketRule("ALICE", "BSC", 1, 1)],
      [new PlotList(["ABCDEF"])],
      "This is plot lottery",
      1621008000000
    );
    const lottery1Id = await lotteryService.getActiveLotteryId();
    await lotteryService.createNewSnapshottingRound(1621008000000);
    await lotteryService.setLotteryState(LotteryState.Snapshotting);
    await lotteryService.saveWalletSnapshot(address, [
      new TokenBalance("ALICE", "BSC", 1000)
    ]);
    await lotteryService.setLotteryState(LotteryState.SnapshottingFinished);
    await lotteryService.issueTickets();
    await lotteryService.setLotteryState(LotteryState.WaitingDraw);
    await lotteryService.draw();
    await lotteryService.setLotteryState(LotteryState.DrawFinished);
    await lotteryService.setLotteryState(LotteryState.Finished);

    await lotteryService.createLottery(
      [new ParticipationRule("ALICE", "BSC", 20)],
      [new TicketRule("ALICE", "BSC", 1, 1)],
      [new PlotRange(100, 200)],
      "This is plot lottery",
      1621008000000
    );
    const lottery2Id = await lotteryService.getActiveLotteryId();
    await lotteryService.createNewSnapshottingRound(1621008000000);
    await lotteryService.setLotteryState(LotteryState.Snapshotting);
    await lotteryService.saveWalletSnapshot(address, [
      new TokenBalance("ALICE", "BSC", 100)
    ]);
    await lotteryService.setLotteryState(LotteryState.SnapshottingFinished);
    await lotteryService.issueTickets();
    await lotteryService.setLotteryState(LotteryState.WaitingDraw);
    await lotteryService.draw();
    await lotteryService.setLotteryState(LotteryState.DrawFinished);
    await lotteryService.setLotteryState(LotteryState.Finished);

    await createTestLottery(lotteryService);
    const lottery3Id = await lotteryService.getActiveLotteryId();
    await lotteryService.createNewSnapshottingRound(1621008000000);
    await lotteryService.setLotteryState(LotteryState.Snapshotting);
    await lotteryService.setLotteryState(LotteryState.SnapshottingFinished);
    await lotteryService.issueTickets();
    await lotteryService.setLotteryState(LotteryState.WaitingDraw);
    await lotteryService.draw();
    await lotteryService.setLotteryState(LotteryState.DrawFinished);
    await lotteryService.setLotteryState(LotteryState.Finished);

    const page1 = await lotteryService.getLotteryRewardsPageForWallet(address, 1, lottery3Id+1);
    const page2 = await lotteryService.getLotteryRewardsPageForWallet(address, 1, page1.oldest_lottery_id);
    const page3 = await lotteryService.getLotteryRewardsPageForWallet(address, 1, page2.oldest_lottery_id);

    expect(page1).toEqual({
      count: 1,
      lotteries: [
        {
          lottery_id: lottery3Id,
          rewards: []
        }
      ],
      oldest_lottery_id: lottery3Id
    })

    expect(page2).toEqual({
      count: 1,
      lotteries: [
        {
          lottery_id: lottery2Id,
          rewards: expect.arrayContaining([
            expect.objectContaining({
              type: "plot",
              details: {
                type: "range_id",
                value: 100
              }
            })
          ])
        }
      ],
      oldest_lottery_id: lottery2Id
    })

    expect(page3).toEqual({
      count: 1,
      lotteries: [
        {
          lottery_id: lottery1Id,
          rewards: expect.arrayContaining([
            expect.objectContaining({
              type: "plot",
              details: {
                type: "id",
                value: "ABCDEF"
              }
            })
          ])
        }
      ],
      oldest_lottery_id: lottery1Id
    })
  }, 20000)
})