const ParticipationRule = require("../js/model/participation-rule");
const TicketRule = require("../js/model/ticket-rule");
const { PlotRange } = require("../js/model/reward-spec");
const { util } = require("postchain-client");

function randomAddress() {
  return util.randomBytes(32).toString("hex");
}

async function createTestLottery(lotteryService) {
  return await lotteryService.createLottery(
    [new ParticipationRule("ALICE", "BSC", 20)],
    [new TicketRule("ALICE", "BSC", 1, 1)],
    [new PlotRange(100, 200)],
    "This is plot lottery",
    1621008000000
  );
}

module.exports = {
  randomAddress,
  createTestLottery
}