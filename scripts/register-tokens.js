require("dotenv").config();

const Token = require("../js/model/token");
const { getLotteryService } = require("./utils");

(async () => {
  const service = await getLotteryService();

  try {
    await service.registerTokens([
      new Token("ALICE", "BSC"),
      new Token("CHR", "BSC"),
      new Token("CHR", "ETHEREUM"),
      new Token("PANCAKE-ALICE-WBNB", "BSC"),
      new Token("BAKERY-ALICE-BAKE", "BSC"),
      new Token("UNISWAP-ALICE-WETH", "ETHEREUM"),
      new Token("UNISWAP-ALICE-USDT", "ETHEREUM"),
    ]);

    console.log("Tokens registered");
  } catch (error) {
    console.error(`Error registering tokens: ${error.message}`);
  }
})()
