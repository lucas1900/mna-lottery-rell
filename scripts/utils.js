const prompt = require("prompt-sync")({sigint: true});
const config = require("./lottery-cli.config");
const LotteryService = require("../js/service/lottery-service");
const { getBlockchainBrid } = require("../js/utils");

async function getLotteryService() {
    const environments = Object.keys(config.environments);

    console.log("Select environment: ")
    environments.forEach(env => console.log(`- ${env}`));

    let isValidEnvironment = false;
    let environment = null;
    do {
        const environmentName = prompt("Enter environment name: ");
        if (environments.includes(environmentName)) {
            isValidEnvironment = true;
            environment = config.environments[environmentName];
        } else {
            console.log(`Invalid environment '${environmentName}'. Please try again.`)
        }
    } while(!isValidEnvironment);

    const brid = environment.brid || await getBlockchainBrid(environment.url, environment.iid);

    return await LotteryService.initialize(environment.url, brid, environment.adminPrivKey);
}

module.exports = {
    getLotteryService
}