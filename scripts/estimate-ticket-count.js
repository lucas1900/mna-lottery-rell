const TicketRule = require("../js/model/ticket-rule");
const Token = require("../js/model/token");
const { PlotRange } = require("../js/model/reward-spec");

const { getLotteryService } = require("./utils");

(async () => {
  const service = await getLotteryService();

  const ticketCount = await service.estimateTicketCount([["PANCAKE-ALICE-WBNB", "BSC", 7.067409]])

  console.log(ticketCount);
})()