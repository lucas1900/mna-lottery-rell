
module.exports = class EVMLotteryDetails {
  constructor(lotteryId, chain, depositContract) {
    this.lotteryId = lotteryId;
    this.chain = chain;
    this.depositContract = depositContract;
  }

  toGTV() {
    return [this.lotteryId, this.chain, this.depositContract];
  }
}