
module.exports = {
	Created: "created",
	Snapshotting: "snapshotting",
	SnapshottingFinished: "snapshotting_finished",
	Paused: "paused",
	Canceled: "canceled",
	WaitingDraw: "waiting_draw",
	DrawFinished: "draw_finished",
	Finished: "finished",
	WaitingSnapshot: "waiting_snapshot"
}