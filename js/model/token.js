
module.exports = class Token {
  constructor(name, chain) {
    this.name = name;
    this.chain = chain;
  }

  toGTV() {
    return [this.name, this.chain];
  }

  isEqual(object) {
    if (typeof object !== typeof this) return false;
    return this.name === object.name && this.chain === object.chain;
  }
}