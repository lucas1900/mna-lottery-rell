
const RewardType = require("./reward-type");

const RewardSpecType = {
  Range: 0,
  List: 1,
  Count: 2
}

class RewardSpec {
}

class PlotList extends RewardSpec {
  constructor(list) {
    super();
    this.list = list;
  }

  toGTV() {
    return [RewardType.Plot, RewardSpecType.List, [this.list]];
  }

  toString() {
    return `Plot list: `;
  }
}

class PlotRange extends RewardSpec {
  constructor(start, end) {
    super();
    this.start = start;
    this.end = end;
  }

  toGTV() {
    return [RewardType.Plot, RewardSpecType.Range, [this.start, this.end]];
  }

  toString() {
    return `Plot range - start: ${this.start}, end: ${this.end}`;
  }
}

class PlotCount extends RewardSpec {
  constructor(count) {
    super();
    this.count = count;
  }

  toGTV() {
    return [RewardType.Plot, RewardSpecType.Count, [this.count]];
  }

  toString() {
    return `Plot count: ${this.count}`
  }
}

module.exports = {
  PlotList,
  PlotRange,
  PlotCount
}
