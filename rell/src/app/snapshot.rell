
// Add snapshot time?
entity snapshot {
	key lottery, wallet, round: snapshot_round, token;
	amount: integer;
	mutable is_processed: boolean;
}

entity snapshot_round {
	key lottery, id: integer;
	time: timestamp;
}

entity token {
	key name, chain: text; // Is chain needed?
}

entity snapshot_token_stat {
	key round: snapshot_round, token;
	amount: integer;
}

function Token(name, chain: text) = require(
	token @? { .name == name, .chain == chain },
	"Token not found: " + name
);

struct token_balance {
	token_name: name;
	chain: name;
	amount: integer;
}

function snapshot_rounds_count(lottery) =
	snapshot_round @? { lottery } ( @omit @group .lottery, @sum 1) ?: 0;

function create_new_snapshotting_round(lottery, time: timestamp): snapshot_round {
	require(
		lottery.state == lottery_state.created 
			or 
		lottery.state == lottery_state.snapshotting_finished,
		"Cannot create new snapshot round while lottery is in state: " + lottery.state
	);
	
	val snapshot_round = create snapshot_round(
		lottery,
		id = snapshot_rounds_count(lottery) + 1,
		time
	);

	set_lottery_state(lottery, lottery_state.waiting_snapshot);
	
	return snapshot_round;
}
	
function current_snapshot_round(lottery) =
	snapshot_round @? { lottery } (@omit @sort_desc .id, snapshot_round) limit 1;

function save_wallet_snapshot(lottery, wallet, balances: list<token_balance>) {
	require(
		lottery.state == lottery_state.snapshotting,
		"Cannot save the snapshot. Lottery is not in snapshotting state"
	);
	
	val round = require(
		current_snapshot_round(lottery),
		"Cannot find current snapshotting round"		
	);
	
	for (balance in balances) {
		create snapshot(
			lottery,
			wallet,
			Token(balance.token_name, balance.chain),
			round,
			amount = balance.amount,
			false
		);
	}
}

function save_snapshot_stats(round: snapshot_round) {
	val snapshot_stats = snapshot @* { round.lottery, round } ( 
		@group .token, @sort_desc @sum .amount
	);

	for ((token, amount) in snapshot_stats) {
		create snapshot_token_stat(round, token, amount);
	}
}

function get_last_completed_snapshot_round(lottery) {
	var current_round = current_snapshot_round(lottery);
	
	if (empty(current_round)) return null;
	
	when (lottery.state) {
		lottery_state.created,
		lottery_state.waiting_snapshot,
		lottery_state.snapshotting,
		lottery_state.paused -> {
			return 
				if (current_round.id == 1) 
					null 
				else 
					snapshot_round @? { lottery, .id == current_round.id - 1};
		}
		else -> {
			return current_round;
		}
	}
}

function get_last_completed_snapshot_round_stats(lottery) {
	var round = get_last_completed_snapshot_round(lottery);

	if (empty(round)) return list<(token: text, chain: text, amount: integer)>();

	return snapshot_token_stat @* { round } ( 
		token = .token.name,
		chain = .token.chain, 
		@sort_desc amount = .amount
	);	
} 

function get_lottery_snapshot_stats(lottery) {
	val snapshot_stats = snapshot_token_stat @* { .round.lottery == lottery } (
		@sort id = .round.id,
		time = .round.time,
		token = .token.name,
		chain = .token.chain,
		@sort_desc amount = .amount	
	);
	
	val lottery_snapshot_stats = list<(
		id: integer, 
		time: timestamp, 
		stats: list<(
			token: text, 
			chain: text, 
			amount: integer
		)>
	)>();
	
	var snapshot: (
		id: integer, 
		time: integer, 
		stats: list<(
			token: text, 
			chain: text, 
			amount: integer
		)>
	)? = null;
	
	for ((id, time, token, chain, amount) in snapshot_stats) {
		if (snapshot == null or snapshot.id != id) {
			snapshot = (
				id = id,
				time = time,
				stats = [(
					token = token, 
					chain = chain, 
					amount = amount
				)]
			);
			lottery_snapshot_stats.add(snapshot);			
		} else {
			snapshot.stats.add((
				token = token,
				chain = chain,
				amount = amount					
			));
		}
	}

	return lottery_snapshot_stats;
}

// Used to save snapshot stats of lotteries which took place before 
// snapshots stats logic was added
function save_lottery_snapshot_stats(lottery) {
	val snapshot_rounds = snapshot_round @* { lottery } (@omit @sort .id, snapshot_round);
	
	for (snapshot_round in snapshot_rounds) {
		save_snapshot_stats(snapshot_round);
	}
}

function get_lotteries_without_snapshot_stats() {
	val lotteries = lottery @* {} ( @omit @sort .id, lottery);
	
	val lotteries_without_snapshot_stats = list<lottery>();
	
	for (lottery in lotteries) {
		val snapshot_stats = snapshot_token_stat@* { .round.lottery == lottery };
		if (snapshot_stats.size() == 0) {
			lotteries_without_snapshot_stats.add(lottery);
		}
	}
	
	return lotteries_without_snapshot_stats;
}
