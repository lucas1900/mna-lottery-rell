module;

import app;

query get_wallet_lottery_state(address: text) {
	val lottery = app.get_active_lottery();
	val status = app.get_wallet_status(address, lottery);
	
	if (empty(status)) return null;
	
	val rewards = list<(
		lottery_id: integer, 
		ticket_id: integer, 
		reward: map<text, gtv>,
		timestamp: integer,
		evm_details: app.evm_details?
	)>();
	
	for ((lottery_id, ticket_id, reward, timestamp, evm_details) in status.rewards) {
		rewards.add((
			lottery_id = lottery_id,
			ticket_id = ticket_id,
			reward = app.map_reward(reward),
			timestamp = timestamp,
			evm_details = evm_details
		));
	}
	
	return (
		master_address = status.master_address,
		lottery = status.lottery,
		wallets = status.wallets,
		rewards = rewards
	).to_gtv_pretty();
}

query get_lottery_state(lottery_id: integer) {
	return app.Lottery(lottery_id).state;
}

query get_wallet_lottery_state_for_round(address: text, lottery_id: integer) {
	return app.get_wallet_status(address, app.Lottery(lottery_id))?.to_gtv_pretty();
}

query get_linked_wallets(address: text) {
	return app.get_linked_wallets(address);
}

query get_winner_info(address: text) {
	return app.get_winner_info(address);
}

query find_winner(lottery_id: integer, address: text) {
	return app.find_winner(app.Lottery(lottery_id), address);
}

query is_master_wallet(address: text) {
	return app.wallet @? { .master_address == address, .address == address }??;
}

query get_master_wallet(address: text) {
	return app.Wallet(address).master_address;
}

query get_wallets_page(page_size: integer, last_rowid: integer) {
	return app.get_wallets_page(page_size, last_rowid);
}

query get_active_lottery_id() {
	return app.get_active_lottery()?.id;
}

query get_active_lottery_details() {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.lottery_to_gtv_pretty(lottery);
}

query get_lottery_details(lottery_id: integer) =
	app.lottery_to_gtv_pretty(app.Lottery(lottery_id));

query get_lotteries_page_desc(page_size: integer, before_lottery_id: integer?) = 
	app.get_lotteries_page_desc(page_size, before_lottery_id ?: integer.MAX_VALUE);

query get_past_lotteries_page_desc(page_size: integer, before_lottery_id: integer?) =
	app.get_past_lotteries_page_desc(page_size, before_lottery_id ?: integer.MAX_VALUE);
	
query get_past_lotteries_count() = app.get_past_lotteries_count();

query get_active_lottery_winners_page(page_size: integer, after_rowid: integer) {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.get_lottery_winners_page(lottery, page_size, after_rowid);
}

query get_lottery_winners_page(lottery_id: integer, page_size: integer, after_rowid: integer) {
	return app.get_lottery_winners_page(app.Lottery(lottery_id), page_size, after_rowid);
}

query estimate_ticket_count_for_active_lottery(balances: list<app.token_balance>) {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.estimate_ticket_count(lottery, balances);
}

query get_active_lottery_wallet_tickets(address: text) {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.get_wallet_tickets(lottery, app.Wallet(address));	
}

query get_active_lottery_tickets() {
	val lottery = app.get_active_lottery();
	if (empty(lottery)) return null;
	return app.get_lottery_tickets(lottery);	
}

query get_registered_tokens() {
	return app.get_registered_tokens();
}

query get_wallet_rewards(address: text) {
	val rewards = app.get_wallet_rewards(app.Wallet(address));
	val response = list<(
		lottery_id: integer, 
		ticket_id: integer, 
		reward: map<text, gtv>, 
		timestamp: integer
	)>();
	
	for ((lottery_id, ticket_id, reward, timestamp, _) in rewards) {
		response.add((
			lottery_id = lottery_id,
			ticket_id = ticket_id,
			reward = app.map_reward(reward),
			timestamp = timestamp
		));
	}
	
	return response;
}

query get_lottery_rewards_page_for_wallet_desc(
	address: text, 
	page_size: integer, 
	before_lottery_id: integer?
) {
	val page = app.get_lottery_rewards_page_for_wallet_desc(
		app.Wallet(address), 
		page_size, 
		before_lottery_id ?: integer.MAX_VALUE
	);
	
	val mapped_lotteries = list<(lottery_id: integer, rewards: list<map<text, gtv>>)>();
	for ((lottery_id, rewards) in page.lotteries) {
		val mapped_rewards = list<map<text, gtv>>();
		for (reward in rewards) {
			mapped_rewards.add(app.map_reward(reward));
		}
		mapped_lotteries.add((
			lottery_id = lottery_id,
			rewards = mapped_rewards
		));
	}
	
	return (
		count = page.count,
		lotteries = mapped_lotteries,
		oldest_lottery_id = page.oldest_lottery_id
	);
}

query get_lottery_snapshot_stats(lottery_id: integer) =
	app.get_lottery_snapshot_stats(app.Lottery(lottery_id));
